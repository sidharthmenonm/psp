document.querySelector(".hamburger-menu").addEventListener('click', function(e) {
  document.querySelector("#navbar").classList.toggle('open');
  document.querySelector(".hamburger-menu").classList.toggle('open')
})

// document.querySelector(".hamburger-menu").addEventListener('click', function(e) {
//   document.querySelector("#navbar").classList.add('open');
//   document.querySelector(".hamburger-menu").classList.add('open');
// })



window.addEventListener("scroll", function() {
  if (window.scrollY > 100) {
    document.querySelector("#navbar .menu").classList.add('sticky')
  } else {
    document.querySelector("#navbar .menu").classList.remove('sticky')
  }
}, false);

// console.log(window.location.pathname);
// window.addEventListener("load", function() {
//   var path = window.location.pathname;
//   if (path == "/" || path == "/index.html") {
//     document.querySelector(".language-selector .ml").classList.add('active');
//   } else if (path == "/index_ml.html") {
//     document.querySelector(".language-selector .en").classList.add('active');
//   }
// })